(function (window) {
    'use strict';

    var todo = {
        'init': $(document).ready(function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                type: 'get',
                url: '/api/index',
                dataType: 'json',
                data: {value: 1},
                success: function (data) {
                    todo.setItems(data);
                    todo.countActiveItems();
                },
            });
        }),
        'save': $(document).on('keypress', '.new-todo', function () {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode === 13) {
                var inputElement = $(this);
                var todoNew = $(inputElement).val();

                if(todoNew.length < 2) {
                    alert('Название должно быть не менее 2х символов!');
                    return false;
                }

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    },
                    type: 'post',
                    url: '/api/save',
                    dataType: 'json',
                    data: {value: todoNew},
                    success: function (data) {
                        var res = '<li><div class="view">\n' +
                            '<input class="toggle" value="'+ data.data.id+ '" type="checkbox">\n' +
                            '<label>' + data.data.name + '</label>\n' +
                            '<button class="destroy" data-id="'+ data.data.id+ '"></button>\n' +
                            '</div></li>';

                        $('.todo-list').append(res);
                        $(inputElement).val('');
                        todo.countActiveItems();
                        console.log(data);
                    },
                });

            }
        }),
        'delete': $(document).on('click', '.destroy', function () {
            var todoId = $(this).attr('data-id');
            var todoElem = $(this);

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                type: 'delete',
                url: '/api/delete',
                dataType: 'json',
                data: {value: todoId},
                success: function (data) {
                    $(todoElem).parent().parent().remove();
                    todo.countActiveItems()
                },
            });
        }),
        'toggleCompleted': $(document).on('click', '.toggle', function () {

            var isCompleted;

            if ($(this).is(':checked')) {
                isCompleted = true;
                $(this).parent().parent().addClass('completed');
            } else {
                isCompleted = false;
                $(this).parent().parent().removeClass('completed');
            }

            var id = $(this).val();

            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                type: 'post',
                url: '/api/update',
                dataType: 'json',
                data: {value: id, 'completed': isCompleted},
                success: function (data) {
                    console.log(data);
                },
            });
            todo.countActiveItems();
        }),
        'listFilters': $(document).on('click', '.filters li a', function () {
            $('.filters li a').removeClass('selected');
            $(this).addClass('selected');
            var filter = $(this).attr('data-name');
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                type: 'post',
                url: '/api/filter',
                dataType: 'json',
                data: {filter: filter},
                success: function (data) {
                    console.log(data);
                    $('.todo-list li').remove();
                    todo.setItems(data);
                },
            });

        }),
		'countActiveItems': function () {
			var all = $('.todo-list li').length;
			var completed = $('.completed').length;
			var active = all - completed;
			$('.todo-count strong').text(active);
		},
        'clearCancelled': $(document).on('click', '.clear-completed', function () {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                },
                type: 'delete',
                url: '/api/clearcompleted',
                dataType: 'json',
                data: {value: 1},
                success: function (data) {
                    console.log(data);
                    $('.todo-list li').remove();
                    todo.setItems(data);
                    todo.countActiveItems();
                },
            });

        }),
        'setItems': function (data) {
            $(data.data).each(function (index, val) {
                var isCompleted = '';
                var checked = '';
                if(val.isCompleted === true) {
                    isCompleted = 'class="completed"';
                    checked = 'checked';
                }

                var todoElement = '<li '+ isCompleted +'>' +
                    '<div class="view">\n' +
                    '<input class="toggle" value="'+val.id+'" type="checkbox" '+ checked +'>\n' +
                    '<label>' + val.name + '</label>\n' +
                    '<button data-id="'+val.id+'" class="destroy"></button>\n' +
                    '</div></li>';
                $('.todo-list').append(todoElement);

            });

        }
    };

    todo.countActiveItems();
})(window);
