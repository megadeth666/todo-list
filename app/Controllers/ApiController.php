<?php
namespace App\Controllers;

use App\Kernel\Auth;
use App\Kernel\Controller;
use App\Repository\TodoListRepository;

class ApiController extends Controller
{
    private $listRepository;

    /**
     * ApiController constructor.
     * @param TodoListRepository $listRepository
     */
    public function __construct(TodoListRepository $listRepository)
    {
        $this->listRepository = $listRepository;
        parent::__construct();
    }

    public function index()
    {
        $items = $this->listRepository->getAllByUserId(Auth::user()->getId());
        echo $this->renderJson(['data' => $items]);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save()
    {
        $value = $this->request['value'];
        $item = $this->listRepository->save(Auth::user()->getId(), $value);
        echo $this->renderJson(['data' => $item]);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function update()
    {
        $id = $this->request['value'];
        $completed = ($this->request['completed'] == 'true');
        $item = $this->listRepository->updateCompleted($id, $completed);
        echo $this->renderJson(['data' => $item]);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete()
    {
        $id = $this->request['value'];
        $this->listRepository->deleteById($id);
        echo $this->renderJson(['data' => $id]);
    }

    public function clearCompleted()
    {
        $this->listRepository->deleteCompletedByUserId(Auth::user()->getId());
        $this->index();

    }

    public function filter()
    {
        $filter = $this->request['filter'];

        $items = $this->listRepository->getByCompletedStatus(Auth::user()->getId(), $filter);
        echo $this->renderJson(['data' => $items]);

    }

}