<?php
namespace App\Controllers;

use App\Kernel\Controller;
use App\Repository\TodoListRepository;

class IndexController extends Controller
{
    public function index()
    {
        echo $this->renderHtml('index');
    }

    public function test()
    {
        echo $this->renderJson(['test' => 'mee']);
    }

}