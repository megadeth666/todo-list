<?php

namespace App\Controllers;

use App\Entities\User;
use App\Kernel\Controller;
use App\Repository\UserRepository;

class AuthController extends Controller
{
    private $userRepository;

    /**
     * AuthController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        parent::__construct();
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function login()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $username = $this->request['username'];
            $password = $this->request['password'];
            /**
             * @var $user User
             */
            $user = $this->userRepository->getByUsername($username);

            if (!$user) {
                echo "Данный пользователь не был найден";
            } else {
                if (md5($password) === $user->getPassword()) {
                    $this->setAuth($user);
                    $this->redirectTo();
                } else {
                    echo "Имя пользователя или пароль не совпадают!";
                }
            }
        }

        echo $this->renderHtml('login');
    }

    /**
     *
     */
    public function logout()
    {
        unset($_SESSION['auth']);
        unset($_COOKIE['username']);
        $this->redirectTo('/auth/login');
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function register()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $username = $this->request['username'];
            $password = $this->request['password'];

            /**
             * @var $user User
             */
            $user = $this->userRepository->getByUsername($username);

            if ($user) {
                echo "Такой пользователь уже есть в системе";
            } else {
                $user = $this->userRepository->save($username, md5($password));
                $this->setAuth($user);
                $this->redirectTo();
            }
        }

        echo $this->renderHtml('register');
    }

    /**
     * @param User $user
     */
    private function setAuth(User $user)
    {
        $_SESSION['auth'] = true;
        $_SESSION['user'] = $user;
    }

    /**
     * @param string $path
     */
    private function redirectTo($path = '/')
    {
        header("Location: http://{$_SERVER['HTTP_HOST']}" . $path);
    }
}