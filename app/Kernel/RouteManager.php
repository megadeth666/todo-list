<?php
namespace App\Kernel;

class RouteManager
{
    use CheckAuth;
    private $request;

    private $server;

    private $defaultControllerName = 'index';

    private $defaultActionName = 'index';

    public function __construct()
    {
        $this->server = $_SERVER;
        $this->request = $_REQUEST;
    }

    /**
     * @return mixed
     * @throws \ReflectionException
     */
    private function setRoute()
    {
        $params = explode('/', $this->server['REQUEST_URI']);
        $controller = $this->getController($params[1]);
        $action = $this->getAction($params);

        $this->check();

        require __DIR__.'/../Controllers/'.$controller.'.php';



        if(!class_exists('\App\Controllers\\'.$controller)) {
            header("HTTP/1.0 404 Not Found");
            die('controller not found');
        };
        $reflectionClass = new \ReflectionClass('\App\Controllers\\'.$controller);
        $className = $reflectionClass->getName();

        $rk = new \ReflectionClass($className);
        $reflectionParams = $rk->getConstructor()->getParameters();
        //die(print_r($rk->getConstructor()->getParameters()));
        $params = [];
        foreach ($reflectionParams as $param) {
            if($param->getType() instanceof \ReflectionNamedType) {
                $namedClass = $param->getClass()->name;
                $params[] =  new $namedClass;
            } else {
                $params[] = $param->getName();
            };
        }

        $classWithNamespase = $reflectionClass->newInstanceArgs($params);

        if(!method_exists($classWithNamespase, $action)) {
            header("HTTP/1.0 404 Not Found");
            die('action not found');
        };
        $result = $classWithNamespase->$action();
        return $result;
    }

    /**
     * @param string $name
     * @return string
     */
    private function getController(string $name) : string
    {
        $controller = $this->defaultControllerName;
        if($name !== '') {
            $controller = $name;
        }
        return ucfirst($controller).'Controller';
    }

    /**
     * @param array $name
     * @return string
     */
    private function getAction(array $name) : string
    {
        if(!isset($name[2])) {
            return $this->defaultActionName;
        }

        $action = strtolower($name[2]);
        return explode('?', $action)[0];
    }

    public function run()
    {
        return $this->setRoute();
    }

}