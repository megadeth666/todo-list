<?php

namespace App\Kernel;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

trait Connection
{
    /**
     * @var $entityManager EntityManager
     */
    protected $entityManager;

    public function __construct()
    {
        $this->entityManager = $this->setEntityManager();
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    private function setEntityManager()
    {
        $dbParams = require(APP."/database.php");

        $paths = array(APP.'/app/Entities');
        $isDevMode = false;

        $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
        $entityManager = EntityManager::create($dbParams, $config);
        return $entityManager;
    }

}