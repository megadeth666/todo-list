<?php
namespace App\Kernel;

use function Composer\Autoload\includeFile;

class Controller
{

    public $request;

    private function setRequest()
    {
        $data = [];
        if($_SERVER['REQUEST_METHOD'] === 'DELETE') {
            $data_input = file_get_contents("php://input");
            $data_params = explode('&', $data_input);

            foreach ($data_params as $data_param) {
                $item = explode('=', $data_param);
                $data[$item[0]] = $item[1];
            }
        } else {
            $data = $_REQUEST;
        }

        return $data;
    }

    public function __construct()
    {

        $this->request = $this->setRequest();
    }

    public function renderHtml(string $filename)
    {
        return $this->getView($filename);
    }

    public function renderJson(array $params)
    {
        return json_encode($params);
    }

    private function getDirectoryViewName()
    {
        $class = get_called_class();
        $explode = explode('\\', $class);
        return str_replace('controller', '', strtolower(end($explode)));
    }

    private function getView($filename)
    {
        $this->getDirectoryViewName();
        ob_start();
        $filepath = VIEWS . '/' . $this->getDirectoryViewName() . '/' . $filename . '.php';

        if (!file_exists($filepath)) {
            return "File View {$filename} not found";
        }

        include($filepath);

        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

}