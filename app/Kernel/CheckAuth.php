<?php
namespace App\Kernel;

trait CheckAuth
{
    public function check()
    {
        if(!isset($_SESSION['auth'])) {
            if($this->server['REQUEST_URI'] != '/auth/login' && $this->server['REQUEST_URI'] != '/auth/register') {
                header("Location: http://{$_SERVER['HTTP_HOST']}/auth/login");
            }
        }
    }
}