<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Template • TodoMVC</title>
    <link rel="stylesheet" href="node_modules/todomvc-common/base.css">
    <link rel="stylesheet" href="node_modules/todomvc-app-css/index.css">
    <!-- CSS overrides - remove if you don't need it -->
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
<div>
    <form action="/auth/login" method="post">
        <label for="username">Логин:</label>
        <input type="text" name="username">
        <br>
        <label for="username">Пароль:</label>
        <input type="password" name="password">
        <br>
        <button type="submit">Войти</button>
    </form>
    <br>
    <a href="/auth/register">Регистрация</a>
</div>
</body>
</html>