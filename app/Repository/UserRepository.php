<?php


namespace App\Repository;


use App\Entities\User;
use App\Kernel\BaseRepository;

class UserRepository extends BaseRepository
{

    /**
     * @param $username
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getByUsername($username)
    {
        $data = $this->entityManager->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.username = :username')
            ->getQuery()
            ->setParameter('username', $username)
            ->getOneOrNullResult();
        return $data;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $data = $this->entityManager->select('t')
            ->from(User::class, 't')
            ->getQuery()
            ->getResult();

        return $data;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        $entity = $this->entityManager->select('t')
            ->from(User::class, 't')
            ->where('t.id = :id')
            ->getQuery()
            ->setParameter('id', $id)
            ->getOneOrNullResult();
        return $entity;
    }


    /**
     * @param $username
     * @param $password
     * @return User
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($username, $password)
    {
        $entity = new User();
        $entity->setusername($username);
        $entity->setPassword($password);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return $entity;
    }


}