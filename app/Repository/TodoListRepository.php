<?php

namespace App\Repository;

use App\Entities\TodoList;
use App\Kernel\BaseRepository;

class TodoListRepository extends BaseRepository
{
    /**
     * @param $user_id
     * @return mixed
     */
    public function getAllByUserId($user_id)
    {
        $data = $this->entityManager->createQueryBuilder()
            ->select('t')
            ->from(TodoList::class, 't')
            ->where('t.userId = :user_id')
            ->getQuery()
            ->setParameter('user_id', $user_id)
            ->getArrayResult();

        return $data;
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $data = $this->entityManager->select('t')
            ->from(TodoList::class, 't')
            ->getQuery()
            ->getResult();

        return $data;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        $entity = $this->entityManager->select('t')
            ->from(TodoList::class, 't')
            ->where('t.id = :id')
            ->getQuery()
            ->setParameter('id', $id)
            ->getSingleResult();
        return $entity;
    }

    /**
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteById($id)
    {
        $entity = $this->entityManager->getRepository(TodoList::class)->find($id);
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    /**
     * @param $id
     * @param $isCompleted
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateCompleted($id, $isCompleted)
    {
        /**
         * @var $entity TodoList
         */
        $entity = $this->entityManager->getRepository(TodoList::class)->find($id);
        $entity->setIsCompleted($isCompleted);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    /**
     * @param $user_id
     * @param $name
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($user_id, $name)
    {
        $entity = new TodoList();
        $entity->setName($name);
        $entity->setUserId($user_id);
        $entity->setIsCompleted(false);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return ['id' => $entity->getId(), 'name' => $entity->getName()];
    }

    /**
     * @param $user_id
     */
    public function deleteCompletedByUserId($user_id)
    {
        $this->entityManager->createQueryBuilder()
            ->delete()
            ->from(TodoList::class, 't')
            ->where('t.userId = :user_id')
            ->andWhere('t.isCompleted = 1')
            ->getQuery()
            ->setParameter('user_id', $user_id)
        ->execute();
    }

    /**
     * @param int $user_id
     * @param string $filter
     * @return array
     */
    public function getByCompletedStatus(int $user_id, string $filter)
    {
        $query = $this->entityManager->createQueryBuilder()
            ->select('t')
            ->from(TodoList::class, 't')
            ->where('t.userId = :user_id')
            ->setParameter('user_id', $user_id);
        if($filter == 'all') {
            return $query->getQuery()->getArrayResult();
        }

        if($filter == 'completed') {
            $query->andWhere('t.isCompleted = 1');
        } else {
            $query->andWhere('t.isCompleted = 0');
        }

        return $query->getQuery()->getArrayResult();
    }
}