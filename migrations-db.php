<?php
$dbParams = require "database.php";
return [
    'dbname' => $dbParams['dbname'],
    'user' => $dbParams['user'],
    'password' => $dbParams['password'],
    'host' => $dbParams['host'],
    'driver' => $dbParams['driver'],
];