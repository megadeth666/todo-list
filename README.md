
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
### Installation

```sh
$ git clone git@bitbucket.org:megadeth666/todo-list.git
$ cd todo-list
$ docker-compose up -d
$ docker-compose exec app ./vendor/bin/doctrine-migrations migrate --no-interaction
$ docker-compose exec app composer install
$ docker-compose exec app bash
$ cd public && npm install
```

Go to the browser [http://localhost:8888](http://localhost:8888)
### Profit!